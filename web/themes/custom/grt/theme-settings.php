<?php
use Drupal\Core\Form\FormStateInterface;
/**
 * @file
 * Custom setting for Grt theme.
 */
function grt_form_system_theme_settings_alter(&$form, FormStateInterface &$form_state, $form_id = NULL) {
  $img_path = $GLOBALS['base_url'] . '/' . drupal_get_path('theme', 'grt') . '/images/grtpro.jpg';
  $img = '<img src="'.$img_path.'" alt="image" />';
  $form['grt'] = [
    '#type'       => 'vertical_tabs',
    '#title'      => '<h3>' . t('GRT Theme Settings') . '</h3>',
    '#default_tab' => 'general',
  ];

  // General settings tab.12
  $form['general'] = [
    '#type'  => 'details',
    '#title' => t('General'),
    '#description' => t('<h3>Thanks for using Grt Theme</h3>Grt is a free Drupal 8 theme designed and developed by Aspire'),
    '#group' => 'grt',
  ];

  // Social tab.
  $form['social'] = [
    '#type'  => 'details',
    '#title' => t('Social'),
    '#description' => t('Social icons settings. These icons appear in header and footer region.'),
    '#group' => 'grt',
  ];

  // Header tab.
  $form['header'] = [
    '#type'  => 'details',
    '#title' => t('Header'),
    '#group' => 'grt',
  ];

  // Sidebar tab.
  $form['sidebar'] = [
    '#type'  => 'details',
    '#title' => t('Sidebar'),
    '#group' => 'grt',
  ];

  // Content tab.
  $form['content'] = [
    '#type'  => 'details',
    '#title' => t('Content'),
    '#group' => 'grt',
  ];

  // Footer tab.
  $form['footer'] = [
    '#type'  => 'details',
    '#title' => t('Footer'),
    '#group' => 'grt',
  ];

  // css tab.
  $form['css'] = [
    '#type'  => 'details',
    '#title' => t('Addtional CSS / Styling'),
    '#group' => 'grt',
  ];

  /*// Support tab.
  $form['support'] = [
    '#type'  => 'details',
    '#title' => t('Support'),
    '#description' => t('For any support related to Grt theme, please post on our <a href="https://www.drupar.com/forum" target="_blank">support forum</a>.'),
    '#group' => 'grt',
  ];
*/
  // Content under general tab.
  $form['general']['general_info'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Theme Info'),
    '#description' => t('GRT Custom theme'),
  ];

  // Settings under social tab.
  // Show or hide all icons.
  $form['social']['all_icons'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Show Social Icons'),
  ];

  $form['social']['all_icons']['all_icons_show'] = [
    '#type'          => 'checkbox',
    '#title'         => t('Show social icons in header and footer'),
    '#default_value' => theme_get_setting('all_icons_show', 'grt'),
    '#description'   => t("Check this option to show social icons in header and footer. Uncheck to hide."),
  ];  

  // Facebook.
    $form['social']['facebook'] = [
    '#type'        => 'details',
    '#title'       => t("Facebook"),
  ];

  $form['social']['facebook']['facebook_url'] = [
    '#type'          => 'textfield',
    '#title'         => t('Facebook Url'),
    '#description'   => t("Enter yours facebook profile or page url. Leave the url field blank to hide this icon."),
    '#default_value' => theme_get_setting('facebook_url', 'grt'),
  ];

  // Twitter.
  $form['social']['twitter'] = [
    '#type'        => 'details',
    '#title'       => t("Twitter"),
  ];

  $form['social']['twitter']['twitter_url'] = [
    '#type'          => 'textfield',
    '#title'         => t('Twitter Url'),
    '#description'   => t("Enter yours twitter page url. Leave the url field blank to hide this icon."),
    '#default_value' => theme_get_setting('twitter_url', 'grt'),
  ];

  // Instagram.
  $form['social']['instagram'] = [
    '#type'        => 'details',
    '#title'       => t("Instagram"),
  ];

  $form['social']['instagram']['instagram_url'] = [
    '#type'          => 'textfield',
    '#title'         => t('Instagram Url'),
    '#description'   => t("Enter yours instagram page url. Leave the url field blank to hide this icon."),
    '#default_value' => theme_get_setting('instagram_url', 'grt'),
  ];

  // Linkedin.
  $form['social']['linkedin'] = [
    '#type'        => 'details',
    '#title'       => t("Linkedin"),
  ];

  $form['social']['linkedin']['linkedin_url'] = [
    '#type'          => 'textfield',
    '#title'         => t('Linkedin Url'),
    '#description'   => t("Enter yours linkedin page url. Leave the url field blank to hide this icon."),
    '#default_value' => theme_get_setting('linkedin_url', 'grt'),
  ];

  // YouTube.
  $form['social']['youtube'] = [
    '#type'        => 'details',
    '#title'       => t("YouTube"),
  ];

  $form['social']['youtube']['youtube_url'] = [
    '#type'          => 'textfield',
    '#title'         => t('YouTube Url'),
    '#description'   => t("Enter yours youtube.com page url. Leave the url field blank to hide this icon."),
    '#default_value' => theme_get_setting('youtube_url', 'grt'),
  ];

  // Vimeo.
  $form['social']['vimeo'] = [
    '#type'        => 'details',
    '#title'       => t("Vimeo"),
  ];

  $form['social']['vimeo']['vimeo_url'] = [
    '#type'          => 'textfield',
    '#title'         => t('YouTube Url'),
    '#description'   => t("Enter yours vimeo.com page url. Leave the url field blank to hide this icon."),
    '#default_value' => theme_get_setting('vimeo_url', 'grt'),
  ];

  // Google Plus.
  $form['social']['gplus'] = [
    '#type'        => 'details',
    '#title'       => t("Google Plus"),
  ];

  $form['social']['gplus']['gplus_url'] = [
    '#type'          => 'textfield',
    '#title'         => t('Google Plus'),
    '#description'   => t("Enter yours youtube.com page url. Leave the url field blank to hide this icon."),
    '#default_value' => theme_get_setting('gplus_url', 'grt'),
  ];

  // Social -> whatsapp.
  $form['social']['whatsapp'] = [
   '#type'        => 'details',
   '#title'       => t("whatsapp"),
  ];
  $form['social']['whatsapp']['whatsapp_url'] = [
   '#type'          => 'textfield',
   '#title'         => t('WhatsApp'),
   '#description'   => t("Enter yours whatsapp url. Leave the url field blank to hide this icon."),
   '#default_value' => theme_get_setting('whatsapp_url', 'grt'),
   ];

   // Social -> telegram.
   $form['social']['telegram'] = [
     '#type'        => 'details',
     '#title'       => t("Telegram"),
   ];
   $form['social']['telegram']['telegram_url'] = [
     '#type'          => 'textfield',
     '#title'         => t('Telegram'),
     '#description'   => t("Enter yours telegram url. Leave the url field blank to hide this icon."),
     '#default_value' => theme_get_setting('telegram_url', 'grt'),
   ];

  // Settings under sidebar.
  // Sidebar -> Frontpage sidebar
  $form['sidebar']['front_sidebars'] = [
    '#type'          => 'fieldset',
    '#title'         => t('Homepage Sidebar'),
  ];
  $form['sidebar']['front_sidebars']['front_sidebar'] = [
    '#type'          => 'checkbox',
    '#title'         => t('Show Sidebars On Homepage'),
    '#default_value' => theme_get_setting('front_sidebar', 'grt'),
    '#description'   => t('Check this option to enable left and right sidebar on homepage.'),
  ];

  // Settings under content tab.
  // Settings under content tab -> Homepage.
  $form['content']['homepage'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Homepage Content'),
    '#description'   => t('Please follow this tutorial to add content on homepage. <a href="https://www.drupar.com/grt-theme-documentation/how-add-content-homepage" target="_blank">How to add content on homepage</a>'),
  ];

  // Content -> Google Fonts.
  $form['content']['google_font_section'] = [
    '#type'          => 'fieldset',
    '#title'         => t('Google Fonts'),
  ];
  $form['content']['google_font_section']['google_font'] = [
    '#type'          => 'select',
    '#title'         => t('Select Google Fonts Location'),
    '#options' => array(
    	'local' => t('Local Self Hosted'),
      'googlecdn' => t('Google CDN Server')
    ),
    '#default_value' => theme_get_setting('google_font', 'grt'),
    '#description'   => t('Grt theme uses following Google fonts: Open Sans, Roboto and Poppins. You can serve these fonts locally or from Google server.'),
  ];

  // Settings under content tab -> Font icons.
  $form['content']['fonticons'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Font Icons'),
  ];
  // Settings under content tab -> FontAwesome icons.
  $form['content']['fonticons']['font_icons'] = [
    '#type'        => 'fieldset',
    '#title'       => t('FontAwesome Font Icons'),
    '#description'   => t('Grt theme has included FontAwesome v4.7 font icons. You can use 600+ icons with Grt theme.<br />Please visit this tutorial page for details. <a href="https://www.drupar.com/custom-shortcodes-set-two/fontawesome-font-icons" target="_blank">How To Use FontAwesome Icons</a>.'),
  ];

  // Settings under content tab -> Shortcodes
  $form['content']['shortcodes'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Shortcodes'),
    '#description'   => t('Grt theme has some custom shortcodes. You can create some styling content using these shortcodes.<br />Please visit this tutorial page for details. <a href="https://www.drupar.com/grt-theme-documentation/grt-shortcodes" target="_blank">Shortcodes in Grt theme</a>.'),
  ];
  // Show user picture in comment.
  $form['content']['comment'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Comment'),
  ];

  $form['content']['comment']['comment_user_pic'] = [
    '#type'          => 'checkbox',
    '#title'         => t('User Picture in comments'),
    '#default_value' => theme_get_setting('comment_user_pic', 'grt'),
    '#description'   => t("Check this option to show user picture in comment. Uncheck to hide."),
  ];

  // Node author picture.
  $form['content']['node'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Node'),
  ];

  $form['content']['node']['node_author_pic'] = [
    '#type'          => 'checkbox',
    '#title'         => t('Node Author Picture'),
    '#default_value' => theme_get_setting('node_author_pic', 'grt'),
    '#description'   => t("Check this option to show node author picture in submitted details. Uncheck to hide."),
  ];

  // Show tags in node submitted.
  $form['content']['node']['node_tags'] = [
    '#type'          => 'checkbox',
    '#title'         => t('Node Tags'),
    '#default_value' => theme_get_setting('node_tags', 'grt'),
    '#description'   => t("Check this option to show node tags (if any) in submitted details. Uncheck to hide."),
  ];

  // Settings under footer tab.
  // Scroll to top.
  $form['footer']['scrolltotop'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Scroll To Top'),
  ];

  $form['footer']['scrolltotop']['scrolltotop_on'] = [
    '#type'          => 'checkbox',
    '#title'         => t('Enable scroll to top feature.'),
    '#default_value' => theme_get_setting('scrolltotop_on', 'grt'),
    '#description'   => t("Check this option to enable scroll to top feature. Uncheck to disable this fearure and hide scroll to top icon."),
  ];

  // Footer -> Copyright.
  $form['footer']['copyright'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Website Copyright Text'),
  ];

  $form['footer']['copyright']['copyright_text'] = [
    '#type'          => 'checkbox',
    '#title'         => t('Show website copyright text in footer.'),
    '#default_value' => theme_get_setting('copyright_text', 'grt'),
    '#description'   => t("Check this option to show website copyright text in footer. Uncheck to hide."),
  ];

  $form['footer']['cookie']['cookie_message'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Show Cookie Consent Message'),
    '#description'   => t('Make your website EU Cookie Law Compliant. According to EU cookies law, websites need to get consent from visitors to store or retrieve cookies.'),
  ];

  $form['css']['css_custom'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Addtional CSS'),
  ];
  $form['css']['css_custom']['css_extra'] = [
    '#type'          => 'checkbox',
    '#title'         => t('Enable Addtional CSS'),
    '#default_value' => theme_get_setting('css_extra', 'grt'),
    '#description'   => t("Check this option to enable additional styling / css. Uncheck to disable this feature."),
  ];
  $form['css']['css_code'] = [
    '#type'          => 'textarea',
    '#title'         => t('Addtional CSS Codes'),
    '#default_value' => theme_get_setting('css_code', 'grt'),
    '#description'   => t('Add your own CSS codes here to customize the appearance of your site.<br />Please refer to this tutorial for detail: <a href="https://www.drupar.com/grt-theme-documentation/custom-css" target="_blank">Custom CSS</a>'),
  ];

// End form.
}
