<?php  

/**  
 * @file  
 * Contains Drupal\testimonials\Form\TestimonialsConfigAdminForm.  
 */  

namespace Drupal\testimonials\Form;  

use Drupal\Core\Form\ConfigFormBase;  
use Drupal\Core\Form\FormStateInterface;  

/**
 * Testimonials admin configuraiton form.
 */
class TestimonialsConfigAdminForm extends ConfigFormBase {  
  /**  
   * {@inheritdoc}  
   */  
  protected function getEditableConfigNames() {  
    return [  
      'testimonials.adminsettings',  
    ];  
  }  

  /**  
   * {@inheritdoc}  
   */  
  public function getFormId() {  
    return 'testimonials_admin_form';  
  }  
  
  /**  
   * {@inheritdoc}  
   */  
  public function buildForm(array $form, FormStateInterface $form_state) {  
    $config = $this->config('testimonials.adminsettings');  

    $form['testimonials_admin_app_id'] = array(  
      '#type' => 'textfield',  
      '#title' => $this->t('App Id'),  
      '#description' => $this->t('Facebook App Id'),  
      '#default_value' => $config->get('testimonials_admin_app_id'),  
      '#required' => TRUE,
    );
    $form['testimonials_admin_app_secret_key'] = array(  
      '#type' => 'textfield',  
      '#title' => $this->t('App Secret Key'),  
      '#description' => $this->t('Facebook App Secret Key'),  
      '#default_value' => $config->get('testimonials_admin_app_secret_key'),  
      '#required' => TRUE,
    );
    $form['testimonials_admin_page_id'] = array(  
      '#type' => 'textfield',  
      '#title' => $this->t('Page Id'),  
      '#description' => $this->t('Facebook Page Id'),  
      '#default_value' => $config->get('testimonials_admin_page_id'),  
      '#required' => TRUE,
    );
    $form['testimonials_admin_page_access_token'] = array(  
      '#type' => 'textarea',  
      '#title' => $this->t('Page Access Token'),  
      '#description' => $this->t('Facebook Page Access Token'),  
      '#default_value' => $config->get('testimonials_admin_page_access_token'),  
      '#required' => TRUE,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**  
   * {@inheritdoc}  
   */  
  public function submitForm(array &$form, FormStateInterface $form_state) {  
    $this->config('testimonials.adminsettings')  
      ->set('testimonials_admin_app_id', trim($form_state->getValue('testimonials_admin_app_id')))
      ->set('testimonials_admin_app_secret_key', trim($form_state->getValue('testimonials_admin_app_secret_key'))) 
      ->set('testimonials_admin_page_id', trim($form_state->getValue('testimonials_admin_page_id'))) 
      ->set('testimonials_admin_page_access_token', trim($form_state->getValue('testimonials_admin_page_access_token')))   
      ->save();  
  }    
}
