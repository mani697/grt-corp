<?php
namespace Drupal\testimonials\Controller;
 
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;
 
class TestimonialsController {
    public function execute() {
       
            $fbReviewsData = $this->getFbGraphReviewsData();
             
            if (!empty($fbReviewsData)) {

            	foreach ($fbReviewsData['data'] as $key => $val) {   

            	   $reviewer_id = "100063619732549"; 

            	   $user_data   = $this->getFbUserData($reviewer_id);        		

		           $data = file_get_contents('http://127.0.0.1/d912/web/sites/default/files/styles/medium/public/fb/profile.png');

				   $file = file_save_data($data, 'public://profile-pic.png');
		           print_r($file); exit;
					$created_time = strtotime($val['created_time']);

		            $node = Node::create([
					  'type'        => 'customer_testimonials',
					  'title'       => 'Facebook Reviews',
					  'field_reviewer_image' => [
					    'target_id' => $file->id()			   
					  ],
					  'field_reviewer_name' => $user_data['first_name']." ".$user_data['last_name'],
					  'field_reviewer_id' => $reviewer_id,			 
					  'field_created_time' => $created_time,
					  'body' => $val['review_text']
					]);

					$node->save();
				}
			}

		echo "Added successfully";
		exit;
    }

    /**
     * Function to get the page reviews
     *
     * @return array
     */
    protected function getFbGraphReviewsData()
    {
        $config 			= \Drupal::config('testimonials.adminsettings');  
    	$fb_page_id 		= trim($config->get('testimonials_admin_page_id'));        
        $page_access_token  = trim($config->get('testimonials_admin_page_access_token'));
       
        $url = "https://graph.facebook.com/v9.0/".$fb_page_id."/ratings?fields=reviewer,review_text,created_time,rating&access_token=".$page_access_token;
      
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);       
        $response = curl_exec($ch); 
        curl_close($ch);       
        $result=json_decode($response, true);

        return $result;
    }

    /**
     * Function to get user access token
     *
     * @return string
     */
    protected function getFbUserAccessToken()
    {              

    	$config 			= \Drupal::config('testimonials.adminsettings');  
    	$app_id 			= trim($config->get('testimonials_admin_app_id'));        
        $app_secret_key  	= trim($config->get('testimonials_admin_app_secret_key'));              
       
		$url = "https://graph.facebook.com/oauth/access_token?client_id=".$app_id."&client_secret=".$app_secret_key."&grant_type=client_credentials";
      
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);       
        $response = curl_exec($ch); 
        curl_close($ch);       
        $result=json_decode($response, true);

        return $result['access_token'];
    }

    /**
     * Function to get user data
     * @param reviewer_id 
     * @return array
     */
    protected function getFbUserData($reviewer_id)
    {              

        $user_access_token = $this->getFbUserAccessToken();          
       
		$url = "https://graph.facebook.com/v9.0/".$reviewer_id."/?fields=first_name,last_name&access_token=".$user_access_token;
      
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);       
        $response = curl_exec($ch); 
        curl_close($ch);       
        $result=json_decode($response, true);

        return $result;
    }
}



