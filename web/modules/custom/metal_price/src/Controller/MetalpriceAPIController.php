<?php
/**
 * @file
 * Contains metal_price\Controller\MetalpriceAPIController.
 */
namespace Drupal\metal_price\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller routines for metal_price routes.
 */
class MetalpriceAPIController extends ControllerBase {

  /**
   * Callback for v1/api/rest/metalprice API method.
   */
  public function get_example( Request $request ) {

    //$response['data'] = 'Some test data to return';
    //$response['method'] = 'GET';
    $response['data'] = \Drupal::database()->select('metal_price', 'n')
            ->fields('n', array('uid', 'metal', 'rate','gram','carat','state'))
            ->execute()->fetchAll();
    $response['message'] = 'Get the data sucessfully';
    return new JsonResponse( $response );
  }

  /**
   * Callback for `my-api/put.json` API method.
   */
  public function put_example( Request $request ) {

    $response['data'] = 'Some test data to return';
    $response['method'] = 'PUT';

    return new JsonResponse( $response );
  }

  /**
   * Callback for v1/api/rest/price API method..
   */
  public function post_example( Request $request ) {
    // This condition checks the `Content-type` and makes sure to 
    // decode JSON string from the request body into array.
    $requestdata = $request->request->all();
    //application raw json 
    if ( 0 === strpos( $request->headers->get( 'Content-Type' ), 'application/json' ) ) {
      $data = json_decode( $request->getContent(), TRUE );
      $request->request->replace( is_array( $data ) ? $data : [] );
      $response['message'] = 'Wronge Content Type';
    } elseif ( 0 === strpos( $request->headers->get( 'Content-Type' ), 'multipart/form-data' )) {
          $result = \Drupal::database()->insert('metal_price')
                      ->fields([
                        'uid' => $requestdata['uid'],
                        'state' => $requestdata['state'], 
                        'metal' => $requestdata['metal'], 
                        'carat' => $requestdata['carat'], 
                        'gram' => $requestdata['gram'], 
                        'rate' => $requestdata['rate']
                      ])
              ->execute();
          $response['message'] = 'Post data Updated';
    }
    return new JsonResponse( $response );
  }

  /**
   * Callback for /v1/api/rest/deleteprice/{uid} API method.
   */
  public function delete_example( Request $request ) {

    $response['data'] = 'Some test data to return';
    $response['method'] = 'DELETE';

    return new JsonResponse( $response );
  }

  /**
   * Callback for /v1/api/rest/metalprice/{uid} API method.
   */
  public function getsingle_example( $uid ) {
    if(!empty($uid)){
      $response['data'] = \Drupal::database()->select('metal_price', 'n')
            ->fields('n', array('uid', 'metal', 'rate','gram','carat','state'))
            ->condition('n.uid', $uid)
            ->execute()->fetchAll();
      $response['message'] = 'Get the data sucessfully';
    }
    return new JsonResponse( $response );
  }

}