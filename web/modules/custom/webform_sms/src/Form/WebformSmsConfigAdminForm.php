<?php  

/**  
 * @file  
 * Contains Drupal\webform_sms\Form\WebformSmsConfigAdminForm.  
 */  

namespace Drupal\webform_sms\Form;  

use Drupal\Core\Form\ConfigFormBase;  
use Drupal\Core\Form\FormStateInterface;  

/**
 * WebformSms admin configuraiton form.
 */
class WebformSmsConfigAdminForm extends ConfigFormBase {  
  /**  
   * {@inheritdoc}  
   */  
  protected function getEditableConfigNames() {  
    return [  
      'webform_sms.adminsettings',  
    ];  
  }  

  /**  
   * {@inheritdoc}  
   */  
  public function getFormId() {  
    return 'webform_sms_admin_form';  
  }  
  
  /**  
   * {@inheritdoc}  
   */  
  public function buildForm(array $form, FormStateInterface $form_state) {  
    $config = $this->config('webform_sms.adminsettings');  

    $form['webform_sms_app_id'] = array(  
      '#type' => 'textfield',  
      '#title' => $this->t('App Id'),  
      '#description' => $this->t('SMS App Id'),  
      '#default_value' => $config->get('webform_sms_app_id'),  
      '#required' => TRUE,
    );
    $form['webform_sms_sub_app_id'] = array(  
      '#type' => 'textfield',  
      '#title' => $this->t('Sub App Id'),  
      '#description' => $this->t('SMS Sub App Id'),  
      '#default_value' => $config->get('webform_sms_sub_app_id'),  
      '#required' => TRUE,
    );
    $form['webform_sms_user_id'] = array(  
      '#type' => 'textfield',  
      '#title' => $this->t('User Id'),  
      '#description' => $this->t('SMS User Id'),  
      '#default_value' => $config->get('webform_sms_user_id'),  
      '#required' => TRUE,
    );
    $form['webform_sms_password'] = array(  
      '#type' => 'textfield',  
      '#title' => $this->t('Password'),  
      '#description' => $this->t('SMS Password'),  
      '#default_value' => $config->get('webform_sms_password'),  
      '#required' => TRUE,
    );
    $form['webform_sms_alert'] = array(  
      '#type' => 'textfield',  
      '#title' => $this->t('Alert'),  
      '#description' => $this->t('SMS Alert'),  
      '#default_value' => $config->get('webform_sms_alert'),  
      '#required' => TRUE,
    );
    $form['webform_sms_url'] = array(  
      '#type' => 'textarea',  
      '#title' => $this->t('Url'),  
      '#description' => $this->t('SMS Url'),  
      '#default_value' => $config->get('webform_sms_url'),  
      '#required' => TRUE,
    );
    $form['webform_sms_sender'] = array(  
      '#type' => 'textfield',  
      '#title' => $this->t('Sender'),  
      '#description' => $this->t('SMS Sender'),  
      '#default_value' => $config->get('webform_sms_sender'),  
      '#required' => TRUE,
    );
    $form['webform_sms_message'] = array(  
      '#type' => 'textarea',  
      '#title' => $this->t('Message'),  
      '#description' => $this->t('SMS Message'),  
      '#default_value' => $config->get('webform_sms_message'),  
      '#required' => TRUE,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**  
   * {@inheritdoc}  
   */  
  public function submitForm(array &$form, FormStateInterface $form_state) {  
    $this->config('webform_sms.adminsettings')  
      ->set('webform_sms_app_id', trim($form_state->getValue('webform_sms_app_id')))
      ->set('webform_sms_sub_app_id', trim($form_state->getValue('webform_sms_sub_app_id'))) 
      ->set('webform_sms_user_id', trim($form_state->getValue('webform_sms_user_id'))) 
      ->set('webform_sms_password', trim($form_state->getValue('webform_sms_password')))
      ->set('webform_sms_alert', trim($form_state->getValue('webform_sms_alert')))
      ->set('webform_sms_url', trim($form_state->getValue('webform_sms_url')))   
      ->set('webform_sms_sender', trim($form_state->getValue('webform_sms_sender')))
      ->set('webform_sms_message', trim($form_state->getValue('webform_sms_message')))
      ->save();  
  }    
}
