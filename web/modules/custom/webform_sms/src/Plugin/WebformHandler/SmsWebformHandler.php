<?php

namespace Drupal\webform_sms\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Send SMS from a webform submission.
 *
 * @WebformHandler(
 *   id = "Send SMS",
 *   label = @Translation("Send SMS"),
 *   category = @Translation("SMS"),
 *   description = @Translation("Send SMS from Webform Submissions."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 * )
 */

class SmsWebformHandler extends WebformHandlerBase {

  /**
   * {@inheritdoc}
   */

  // Function to be fired after submitting the Webform.
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {

    // Get an array of the values from the submission.
    $values = $webform_submission->getData();

    $config       = \Drupal::config('webform_sms.adminsettings');  
    $message      = $config->get('webform_sms_message');

    $this->sendSms($message, $values['phone_number']);
    
  }

    /**
     * Function to send sms
     * @return array
     */
    public function sendSms($content, $mobileNumber)
    {
        $config       = \Drupal::config('webform_sms.adminsettings');  
       
        $appId        = $config->get('webform_sms_app_id');
        $subAppId     = $config->get('webform_sms_sub_app_id');
        $userId       = $config->get('webform_sms_user_id');
        $password     = $config->get('webform_sms_password');
        $alert        = $config->get('webform_sms_alert');
        $smsUrl       = $config->get('webform_sms_url');
        $sender       = $config->get('webform_sms_sender');
        $content      =  urlencode($content);

        $url = $smsUrl.'userId='.$userId.'&pass='.$password.'&appid='.$appId.'&subappid='.$subAppId.'&contenttype=1'.'&to='.$mobileNumber.'&from='.$sender.'&text='.$content.'&selfid=true&alert='.$alert.'&dlrreq=true';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $this->_statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($this->_statusCode=="200") {
            $status = "Success";
        } else {
            $status = "Failed";
        }
        
    }

}

